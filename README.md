# Staging

This is a repository for shaders/resources either not designed for or not ready for a full release. There's absolutely no guarantees that they work and no assurance that they can be used. 