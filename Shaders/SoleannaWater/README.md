This is the water shader I made for Soleanna. It's designed to be used on a mesh facing up (like a Unity quad) with a mirror component.  The shader will apply a specular distribution to give the water a proper realistic reflection. The mesh's vertex colours will be used as baked lighting, so you can bake shadows into the mesh. 

The mirror component should set the internal `_ReflectionTex0`/`_ReflectionTex1` parameters.
Specular F0 determines the intensity of the reflectionl tune to your taste.
Water01/02 define the normal maps for the water distortion.
dfg-ms defines the specular distribution and must be the same dfg-ms as provided in the package.
Water Depth Colour sets the colour of the water as it spreads further from the horizon. 
Axis Parameters set the scroll speed for the water distortion. 
