// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "PSU Alpha"
{
	Properties
	{
		_MainTex("MainTex", 2D) = "white" {}
		[Enum(UnityEngine.Rendering.BlendMode)]_SrcBB("SrcBB", Int) = 0
		[Enum(UnityEngine.Rendering.BlendMode)]_DstBB("DstBB", Int) = 1
		[ToggleUI]_UseSecondUVLayer("Use Second UV Layer", Float) = 0
		_SecondUVSpeed("Second UV Speed", Vector) = (0,0,0,0)
		[Enum(UnityEngine.Rendering.CullMode)] _CullMode("Cull Mode", Float) = 0
    [Enum(UnityEngine.Rendering.BlendOp)]
    _ParticleBlendOp("BlendOp", Float) = 0 // None

	}
	
	SubShader
	{
		
		
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
	LOD 100

		CGINCLUDE
		#pragma target 3.0
		ENDCG
		Blend [_SrcBB] [_DstBB]
    	BlendOp [_ParticleBlendOp] 
		AlphaToMask Off
		Cull [_CullMode]
		ColorMask RGBA
		ZWrite Off
		ZTest LEqual
		Offset 0 , 0
		
		
		
		Pass
		{
			Name "Unlit"
			Tags { "LightMode"="ForwardBase" }
			CGPROGRAM

			

			#ifndef UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX
			//only defining to not throw compilation error over Unity 5.5
			#define UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input)
			#endif
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_instancing
			#include "UnityCG.cginc"
			#include "UnityShaderVariables.cginc"
			#define ASE_NEEDS_FRAG_COLOR


			struct appdata
			{
				float4 vertex : POSITION;
			centroid	float4 color : COLOR;
				float4 ase_texcoord1 : TEXCOORD1;
				float4 ase_texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				float3 worldPos : TEXCOORD0;
#endif
				float4 ase_texcoord1 : TEXCOORD1;
			centroid	float4 ase_color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			uniform int _SrcBB;
			uniform int _DstBB;
			uniform float _UseSecondUVLayer;
			uniform sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform float4 _SecondUVSpeed;
			float VColFix( float In0 )
			{
				#define COLOR0 COLOR0_centroid
			}
			

			
			v2f vert ( appdata v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				UNITY_TRANSFER_INSTANCE_ID(v, o);

				o.ase_texcoord1.xy = v.ase_texcoord1.xy;
				o.ase_texcoord1.zw = v.ase_texcoord.xy;
				o.ase_color = v.color;
				float3 vertexValue = float3(0, 0, 0);
				#if ASE_ABSOLUTE_VERTEX_POS
				vertexValue = v.vertex.xyz;
				#endif
				vertexValue = vertexValue;
				#if ASE_ABSOLUTE_VERTEX_POS
				v.vertex.xyz = vertexValue;
				#else
				v.vertex.xyz += vertexValue;
				#endif
				o.vertex = UnityObjectToClipPos(v.vertex);

#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
#endif
				return o;
			}
			
			fixed4 frag (v2f i ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(i);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);
				fixed4 finalColor;
#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				float3 WorldPosition = i.worldPos;
#endif
				float2 uv124 = i.ase_texcoord1.xy * _MainTex_ST.xy + frac( ( _Time.y * _SecondUVSpeed ) ).xy;
				float4 ifLocalVar26 = 0;
				UNITY_BRANCH 
				if( _UseSecondUVLayer > 0.0 )
				ifLocalVar26 = tex2D( _MainTex, uv124 );
				float2 uv06 = i.ase_texcoord1.zw * _MainTex_ST.xy + frac( ( _Time.y * _MainTex_ST.zw ) );
				float4 MainTex_var28 = ( ifLocalVar26 + tex2D( _MainTex, uv06 ) );
				float3 linearToGamma15 = LinearToGammaSpace( MainTex_var28.rgb );
				float3 gammaToLinear16 = GammaToLinearSpace( ( float4( linearToGamma15 , 0.0 ) * i.ase_color ).rgb );
				float4 temp_output_20_0 = ( MainTex_var28 * i.ase_color );
				float4 lerpResult19 = lerp( float4( gammaToLinear16 , 0.0 ) , temp_output_20_0 , 0.25);
				float4 appendResult22 = (float4(( lerpResult19 * unity_ColorSpaceDouble ).rgb , (temp_output_20_0).a));
				
				
				finalColor = appendResult22;
				return finalColor;
			}
			ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
/*ASEBEGIN
Version=18301
2110;191;1606;1166;3730.816;961.1453;1;True;False
Node;AmplifyShaderEditor.TimeNode;33;-3335.965,-763.6041;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector4Node;32;-3302.816,-597.1453;Inherit;False;Property;_SecondUVSpeed;Second UV Speed;4;0;Create;True;0;0;False;0;False;0,0,0,0;0,0,0,0;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TimeNode;12;-3289.853,-150.6642;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;34;-3075.281,-795.0676;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TextureTransformNode;11;-3284.169,-331.1276;Inherit;False;3;False;1;0;SAMPLER2D;_Sampler011;False;2;FLOAT2;0;FLOAT2;1
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;9;-3029.169,-182.1277;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.FractNode;35;-2900.202,-709.7639;Inherit;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;24;-2622.321,-388.7893;Inherit;False;1;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FractNode;8;-2854.09,-96.82393;Inherit;False;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;6;-2636.247,-138.6007;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;27;-2445.387,-608.0996;Inherit;False;Property;_UseSecondUVLayer;Use Second UV Layer;3;0;Create;True;0;0;False;1;ToggleUI;False;0;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;25;-2289.698,-393.5532;Inherit;True;Property;_MainTex1;MainTex;0;0;Create;True;0;0;False;0;False;-1;None;3c24dada5864b9749bb86a844e6d6236;True;1;False;white;Auto;False;Instance;3;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ConditionalIfNode;26;-1969.884,-488.9127;Inherit;False;True;5;0;FLOAT;0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;3;-2292.447,-181.0007;Inherit;True;Property;_MainTex;MainTex;0;0;Create;True;0;0;False;0;False;-1;None;3c24dada5864b9749bb86a844e6d6236;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;30;-1752.816,-374.1453;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;28;-1607.362,-374.4163;Inherit;False;MainTex_var;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;29;-606.9691,-451.8839;Inherit;False;28;MainTex_var;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.VertexColorNode;1;-936.1505,-35.29517;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LinearToGammaNode;15;-673.3176,-270.0507;Inherit;False;0;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;4;-477.4884,-129.1814;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;21;-443.8711,119.4721;Inherit;False;Constant;_Float0;Float 0;3;0;Create;True;0;0;False;0;False;0.25;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GammaToLinearNode;16;-302.7473,-278.5003;Inherit;False;0;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;-444.9152,25.49969;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorSpaceDouble;5;-776,382;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;19;-104.5263,-26.70719;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ComponentMaskNode;23;-218.2095,191.7297;Inherit;False;False;False;False;True;1;0;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;17;11.5857,333.3963;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.DynamicAppendNode;22;230.3289,40.28401;Inherit;False;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.IntNode;14;-1255.105,-886.1992;Inherit;False;Property;_DstBB;DstBB;2;1;[Enum];Create;True;0;1;UnityEngine.Rendering.BlendMode;True;0;False;1;1;0;1;INT;0
Node;AmplifyShaderEditor.IntNode;13;-1255.105,-960.1992;Inherit;False;Property;_SrcBB;SrcBB;1;1;[Enum];Create;True;0;1;UnityEngine.Rendering.BlendMode;True;0;False;0;1;0;1;INT;0
Node;AmplifyShaderEditor.CustomExpressionNode;18;-1108.61,-961.8721;Inherit;False;#define COLOR0 COLOR0_centroid;1;False;1;True;In0;FLOAT;0;In;;Inherit;False;VColFix;False;True;0;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;2;576.927,42.96206;Float;False;True;-1;2;ASEMaterialInspector;100;1;PSU Alpha;0770190933193b94aaa3065e307002fa;True;Unlit;0;0;Unlit;2;True;3;1;True;13;10;True;14;0;1;False;-1;0;False;-1;True;0;False;-1;0;False;-1;False;False;False;False;False;False;True;0;False;-1;True;2;False;-1;True;True;True;True;True;0;False;-1;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;True;2;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;2;RenderType=Transparent=RenderType;Queue=Transparent=Queue=0;True;2;0;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;1;LightMode=ForwardBase;False;0;;0;0;Standard;1;Vertex Position,InvertActionOnDeselection;1;0;1;True;False;;0
WireConnection;34;0;33;2
WireConnection;34;1;32;0
WireConnection;9;0;12;2
WireConnection;9;1;11;1
WireConnection;35;0;34;0
WireConnection;24;0;11;0
WireConnection;24;1;35;0
WireConnection;8;0;9;0
WireConnection;6;0;11;0
WireConnection;6;1;8;0
WireConnection;25;1;24;0
WireConnection;26;0;27;0
WireConnection;26;2;25;0
WireConnection;3;1;6;0
WireConnection;30;0;26;0
WireConnection;30;1;3;0
WireConnection;28;0;30;0
WireConnection;15;0;29;0
WireConnection;4;0;15;0
WireConnection;4;1;1;0
WireConnection;16;0;4;0
WireConnection;20;0;29;0
WireConnection;20;1;1;0
WireConnection;19;0;16;0
WireConnection;19;1;20;0
WireConnection;19;2;21;0
WireConnection;23;0;20;0
WireConnection;17;0;19;0
WireConnection;17;1;5;0
WireConnection;22;0;17;0
WireConnection;22;3;23;0
WireConnection;2;0;22;0
ASEEND*/
//CHKSM=E1B1DA11B1608DD857FDC232AEA81B96DC12F6B4