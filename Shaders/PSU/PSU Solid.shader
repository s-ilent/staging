// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "PSU Solid"
{
	Properties
	{
		_MainTex("MainTex", 2D) = "white" {}
		[NoScaleOffset][SingleLineTexture]_blueNoise1("blueNoise", 2D) = "white" {}
		[ToggleUI]_ZWrite("ZWrite", Float) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		ZWrite [_ZWrite]
		CGPROGRAM
		#include "UnityPBSLighting.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#pragma target 3.0
		#pragma surface surf StandardCustomLighting keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
			float4 screenPos;
		centroid	float4 vertexColor : COLOR;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform float _ZWrite;
		uniform sampler2D _MainTex;
		float4 _MainTex_TexelSize;
		uniform float4 _MainTex_ST;
		uniform sampler2D _blueNoise1;
		float4 _blueNoise1_TexelSize;


		float VColFix( float In0 )
		{
			#define COLOR0 COLOR0_centroid
		}


		float2 UnStereo( float2 UV )
		{
			#if UNITY_SINGLE_PASS_STEREO
			float4 scaleOffset = unity_StereoScaleOffset[ unity_StereoEyeIndex ];
			UV.xy = (UV.xy - scaleOffset.zw) / scaleOffset.xy;
			#endif
			return UV;
		}


		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			c.rgb = 0;
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			float2 uv0_MainTex = i.uv_texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
			float2 temp_output_6_0_g21 = ( uv0_MainTex * (_MainTex_TexelSize).zw );
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float2 UV22_g2 = ase_screenPosNorm.xy;
			float2 localUnStereo22_g2 = UnStereo( UV22_g2 );
			float4 tex2DNode4_g1 = tex2D( _blueNoise1, ( ( floor( ( float4( localUnStereo22_g2, 0.0 , 0.0 ) * _ScreenParams ) ) + float4( floor( ( _Time.y * (_blueNoise1_TexelSize).zw ) ), 0.0 , 0.0 ) ) * _blueNoise1_TexelSize ).xy );
			float4 blueNoise10 = tex2DNode4_g1;
			float mulTime28 = _Time.y * 3.0;
			float4 lerpResult34 = lerp( blueNoise10 , float4( (blueNoise10).gb, 0.0 , 0.0 ) , saturate( floor( ( ( mulTime28 % 0.3333333 ) * 12.0 ) ) ));
			float4 lerpResult45 = lerp( lerpResult34 , float4( (blueNoise10).br, 0.0 , 0.0 ) , saturate( floor( ( ( mulTime28 % 0.3333333 ) * 6.0 ) ) ));
			float2 appendResult29_g21 = (float2(ddx( i.uv_texcoord.x ) , ddy( i.uv_texcoord.y )));
			float2 clampResult34_g21 = clamp( ( 0.125 / ( abs( appendResult29_g21 ) * (_MainTex_TexelSize).zw ) ) , float2( -1,-1 ) , float2( 1,1 ) );
			float4 tex2DNode1 = tex2D( _MainTex, ( _MainTex_TexelSize * float4( ( floor( temp_output_6_0_g21 ) + ( frac( temp_output_6_0_g21 ) + ( 1.0 * lerpResult45.rg * clampResult34_g21 ) ) ), 0.0 , 0.0 ) ).xy );
			float3 linearToGamma76 = LinearToGammaSpace( tex2DNode1.rgb );
			float3 gammaToLinear77 = GammaToLinearSpace( ( float4( linearToGamma76 , 0.0 ) * i.vertexColor ).rgb );
			float4 lerpResult81 = lerp( float4( gammaToLinear77 , 0.0 ) , ( tex2DNode1 * i.vertexColor ) , 0.25);
			float4 temp_output_78_0 = ( lerpResult81 * unity_ColorSpaceDouble );
			o.Albedo = temp_output_78_0.rgb;
			o.Emission = temp_output_78_0.rgb;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18301
1867;1031;1606;1000;2930.738;739.4734;1.628306;True;False
Node;AmplifyShaderEditor.SimpleTimeNode;28;-3200.375,298.1004;Inherit;False;1;0;FLOAT;3;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;84;-1560.651,-317.9217;Inherit;False;GetBlueNoise;1;;1;3cbd807991120e84ab1d85f5cff4d57f;0;0;1;COLOR;0
Node;AmplifyShaderEditor.SimpleRemainderNode;29;-2997.938,269.9005;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0.3333333;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;10;-1358.349,-331.0784;Inherit;False;blueNoise;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleRemainderNode;39;-3009.122,393.9851;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0.3333333;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;37;-2815.233,273.2088;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;12;False;1;FLOAT;0
Node;AmplifyShaderEditor.FloorOpNode;33;-2657.821,275.8238;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;38;-2822.418,396.2935;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;6;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;7;-2889.967,22.97539;Inherit;False;10;blueNoise;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.ComponentMaskNode;35;-2679.739,84.5914;Inherit;False;False;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.FloorOpNode;40;-2655.77,419.6392;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;41;-2528.77,311.6391;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;34;-2290.234,123.5157;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;42;-2536.77,433.6392;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SwizzleNode;44;-2668.829,176.3506;Inherit;False;FLOAT2;2;0;2;3;1;0;COLOR;0,0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.LerpOp;45;-2114.524,258.0748;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.TexturePropertyNode;6;-1893,-240.3333;Inherit;True;Property;_MainTex;MainTex;0;0;Fetch;True;0;0;True;0;False;None;01f5ab08fb87e414aae1b7b811a41875;False;white;Auto;Texture2D;-1;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.FunctionNode;73;-1578,-45.33333;Inherit;False;DitheredSampleUVs;-1;;21;1bae100dee9759348839076f4e1c8d64;0;3;18;SAMPLER2D;;False;20;FLOAT2;0,0;False;21;FLOAT2;0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SamplerNode;1;-1137,-57;Inherit;True;Property;_MainTex;MainTex;0;0;Create;True;0;0;False;0;False;-1;None;01f5ab08fb87e414aae1b7b811a41875;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.VertexColorNode;3;-1010,144;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LinearToGammaNode;76;-784.0773,-96.34737;Inherit;False;0;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;4;-568.7422,-85.13528;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GammaToLinearNode;77;-423.2776,-95.94727;Inherit;False;0;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;79;-576.6551,13.01138;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;82;-544.6551,168.0113;Inherit;False;Constant;_Float0;Float 0;3;0;Create;True;0;0;False;0;False;0.25;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorSpaceDouble;5;-357.481,289.0961;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;81;-329.6552,140.0113;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LightAttenuation;75;-633.8096,-168.8695;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;78;-96.63506,186.9879;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;1,1,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CustomExpressionNode;83;-724.1428,349.7242;Inherit;False;#define COLOR0 COLOR0_centroid;1;False;1;True;In0;FLOAT;0;In;;Inherit;False;VColFix;False;True;0;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;85;-802.4833,535.9337;Inherit;False;Property;_ZWrite;ZWrite;3;0;Create;True;0;1;UnityEngine.Rendering.StencilOp;True;1;ToggleUI;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;144,145.6;Float;False;True;-1;2;ASEMaterialInspector;0;0;CustomLighting;PSU Solid;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;True;85;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;29;0;28;0
WireConnection;10;0;84;0
WireConnection;39;0;28;0
WireConnection;37;0;29;0
WireConnection;33;0;37;0
WireConnection;38;0;39;0
WireConnection;35;0;7;0
WireConnection;40;0;38;0
WireConnection;41;0;33;0
WireConnection;34;0;7;0
WireConnection;34;1;35;0
WireConnection;34;2;41;0
WireConnection;42;0;40;0
WireConnection;44;0;7;0
WireConnection;45;0;34;0
WireConnection;45;1;44;0
WireConnection;45;2;42;0
WireConnection;73;18;6;0
WireConnection;73;20;45;0
WireConnection;1;1;73;0
WireConnection;76;0;1;0
WireConnection;4;0;76;0
WireConnection;4;1;3;0
WireConnection;77;0;4;0
WireConnection;79;0;1;0
WireConnection;79;1;3;0
WireConnection;81;0;77;0
WireConnection;81;1;79;0
WireConnection;81;2;82;0
WireConnection;78;0;81;0
WireConnection;78;1;5;0
WireConnection;0;0;78;0
WireConnection;0;2;78;0
ASEEND*/
//CHKSM=AD621D20C1E8825998D7A3FC111CBD3EB243F4C7