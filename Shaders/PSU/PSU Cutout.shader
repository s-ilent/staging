// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "PSU Cutout"
{
	Properties
	{
		[Toggle(BLOOM)] BLOOM("Wind Sway", Float) = 0
		_BranchPhase("Branch Phase", Float) = 0
		_MainTex("MainTex", 2D) = "white" {}
		_EdgeFlutter("Edge Flutter", Float) = 0
		_PrimaryFactor("Primary Factor", Float) = 0
		_SecondaryFactor("Secondary Factor", Float) = 0
		[NoScaleOffset][SingleLineTexture]_blueNoise1("blueNoise", 2D) = "white" {}
		[ToggleUI]_VertexColourAffectsTransparency("Vertex Colour Affects Transparency", Float) = 0
		[ToggleUI]_SharpenAlpha("Sharpen Alpha", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "AlphaTest+0" "IsEmissive" = "true"  }
		Cull Off
		AlphaToMask On
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#pragma shader_feature BLOOM
		#include "TerrainEngine.cginc"
		struct Input
		{
			float3 worldPos;
			float2 uv_texcoord;
			float4 screenPos;
		centroid	float4 vertexColor : COLOR;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform float _BranchPhase;
		uniform float _EdgeFlutter;
		uniform float _PrimaryFactor;
		uniform float _SecondaryFactor;
		uniform sampler2D _MainTex;
		float4 _MainTex_TexelSize;
		uniform float4 _MainTex_ST;
		uniform sampler2D _blueNoise1;
		float4 _blueNoise1_TexelSize;
		uniform float _SharpenAlpha;
		uniform float _VertexColourAffectsTransparency;


		float VColFix( float In0 )
		{
			#define COLOR0 COLOR0_centroid
		}


		float noiseSwitched( float4 noise , float4 noise_TexelSize )
		{
			int no = (_Time.y % (1.0/8.0))*32;
			return noise[no];
		}


		float3 WaveGrass88( float4 vertex , float4 waveAndDistance , float waveAmount )
		{
			// Setup to compensate for missing terrain data
			waveAndDistance.x += _Time.x * waveAndDistance.w;
			float4 _waveXSize = float4(0.012, 0.02, 0.06, 0.024) * waveAndDistance.y;
			float4 _waveZSize = float4 (0.006, .02, 0.02, 0.05) * waveAndDistance.y;
			float4 waveSpeed = float4 (0.3, .5, .4, 1.2) * 4;
			float4 _waveXmove = float4(0.012, 0.02, -0.06, 0.048) * 2;
			float4 _waveZmove = float4 (0.006, .02, -0.02, 0.1);
			float4 waves;
			waves = vertex.x * _waveXSize;
			waves += vertex.z * _waveZSize;
			// Add in time to model them over time
			waves += waveAndDistance.x * waveSpeed;
			float4 s, c;
			waves = frac (waves);
			FastSinCos (waves, s,c);
			s = s * s;
			s = s * s;
			s = s * waveAmount;
			float3 waveMove = float3 (0,0,0);
			waveMove.x = dot (s, _waveXmove);
			waveMove.z = dot (s, _waveZmove);
			//vertex.xz -= waveMove.xz * waveAndDistance.z;
			float3 final = vertex.xyz;
			final.xz -= waveMove.xz * waveAndDistance.z;
			//final -= vertex.xyz;
			return final;
		}


		float2 UnStereo( float2 UV )
		{
			#if UNITY_SINGLE_PASS_STEREO
			float4 scaleOffset = unity_StereoScaleOffset[ unity_StereoEyeIndex ];
			UV.xy = (UV.xy - scaleOffset.zw) / scaleOffset.xy;
			#endif
			return UV;
		}


		float SharpenAlpha68( float x , float cutout )
		{
			return (x-cutout) / max(fwidth(x), 0.001) + cutout;
		}


		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float4 ase_vertex4Pos = v.vertex;
			float3 ase_worldPos = mul( unity_ObjectToWorld, v.vertex );
			float4 vertex88 = float4( ase_worldPos , 0.0 );
			float4 appendResult17 = (float4(_BranchPhase , _EdgeFlutter , _PrimaryFactor , _SecondaryFactor));
			float4 waveAndDistance88 = appendResult17;
			float waveAmount88 = 1.0;
			float3 localWaveGrass88 = WaveGrass88( vertex88 , waveAndDistance88 , waveAmount88 );
			float4 transform96 = mul(unity_WorldToObject,float4( localWaveGrass88 , 0.0 ));
			#ifdef BLOOM
				float4 staticSwitch99 = transform96;
			#else
				float4 staticSwitch99 = ase_vertex4Pos;
			#endif
			v.vertex.xyz = staticSwitch99.xyz;
		}

		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			float2 uv0_MainTex = i.uv_texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
			float2 temp_output_6_0_g19 = ( uv0_MainTex * (_MainTex_TexelSize).zw );
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float2 UV22_g26 = ase_screenPosNorm.xy;
			float2 localUnStereo22_g26 = UnStereo( UV22_g26 );
			float4 tex2DNode4_g25 = tex2D( _blueNoise1, ( ( floor( ( float4( localUnStereo22_g26, 0.0 , 0.0 ) * _ScreenParams ) ) + float4( floor( ( _Time.y * (_blueNoise1_TexelSize).zw ) ), 0.0 , 0.0 ) ) * _blueNoise1_TexelSize ).xy );
			float4 temp_output_103_0 = tex2DNode4_g25;
			float4 blueNoise34 = temp_output_103_0;
			float mulTime55 = _Time.y * 3.0;
			float4 lerpResult51 = lerp( blueNoise34 , float4( (blueNoise34).gb, 0.0 , 0.0 ) , saturate( floor( ( ( mulTime55 % 0.3333333 ) * 12.0 ) ) ));
			float4 lerpResult52 = lerp( lerpResult51 , float4( (blueNoise34).br, 0.0 , 0.0 ) , saturate( floor( ( ( mulTime55 % 0.3333333 ) * 6.0 ) ) ));
			float2 appendResult29_g19 = (float2(ddx( i.uv_texcoord.x ) , ddy( i.uv_texcoord.y )));
			float2 clampResult34_g19 = clamp( ( 0.125 / ( abs( appendResult29_g19 ) * (_MainTex_TexelSize).zw ) ) , float2( -1,-1 ) , float2( 1,1 ) );
			float4 tex2DNode1 = tex2D( _MainTex, ( _MainTex_TexelSize * float4( ( floor( temp_output_6_0_g19 ) + ( frac( temp_output_6_0_g19 ) + ( 1.0 * lerpResult52.rg * clampResult34_g19 ) ) ), 0.0 , 0.0 ) ).xy );
			float ifLocalVar22 = 0;
			if( _VertexColourAffectsTransparency <= 0.0 )
				ifLocalVar22 = tex2DNode1.a;
			else
				ifLocalVar22 = ( tex2DNode1.a * i.vertexColor.a );
			float x68 = ifLocalVar22;
			float cutout68 = 0.25;
			float localSharpenAlpha68 = SharpenAlpha68( x68 , cutout68 );
			float4 temp_cast_28 = (localSharpenAlpha68).xxxx;
			float4 temp_cast_29 = (ifLocalVar22).xxxx;
			float4 temp_output_29_0 = ( temp_cast_29 - ( temp_output_103_0 * sqrt( ( 1.0 - ( -0.003921569 + ifLocalVar22 ) ) ) ) );
			float4 temp_cast_30 = (ifLocalVar22).xxxx;
			float4 ifLocalVar67 = 0;
			if( _SharpenAlpha <= 0.0 )
				ifLocalVar67 = temp_output_29_0;
			else
				ifLocalVar67 = temp_cast_28;
			c.rgb = 0;
			c.a = saturate( ifLocalVar67 ).r;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			float2 uv0_MainTex = i.uv_texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
			float2 temp_output_6_0_g19 = ( uv0_MainTex * (_MainTex_TexelSize).zw );
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float2 UV22_g26 = ase_screenPosNorm.xy;
			float2 localUnStereo22_g26 = UnStereo( UV22_g26 );
			float4 tex2DNode4_g25 = tex2D( _blueNoise1, ( ( floor( ( float4( localUnStereo22_g26, 0.0 , 0.0 ) * _ScreenParams ) ) + float4( floor( ( _Time.y * (_blueNoise1_TexelSize).zw ) ), 0.0 , 0.0 ) ) * _blueNoise1_TexelSize ).xy );
			float4 temp_output_103_0 = tex2DNode4_g25;
			float4 blueNoise34 = temp_output_103_0;
			float mulTime55 = _Time.y * 3.0;
			float4 lerpResult51 = lerp( blueNoise34 , float4( (blueNoise34).gb, 0.0 , 0.0 ) , saturate( floor( ( ( mulTime55 % 0.3333333 ) * 12.0 ) ) ));
			float4 lerpResult52 = lerp( lerpResult51 , float4( (blueNoise34).br, 0.0 , 0.0 ) , saturate( floor( ( ( mulTime55 % 0.3333333 ) * 6.0 ) ) ));
			float2 appendResult29_g19 = (float2(ddx( i.uv_texcoord.x ) , ddy( i.uv_texcoord.y )));
			float2 clampResult34_g19 = clamp( ( 0.125 / ( abs( appendResult29_g19 ) * (_MainTex_TexelSize).zw ) ) , float2( -1,-1 ) , float2( 1,1 ) );
			float4 tex2DNode1 = tex2D( _MainTex, ( _MainTex_TexelSize * float4( ( floor( temp_output_6_0_g19 ) + ( frac( temp_output_6_0_g19 ) + ( 1.0 * lerpResult52.rg * clampResult34_g19 ) ) ), 0.0 , 0.0 ) ).xy );
			float4 mainTex78 = tex2DNode1;
			float3 linearToGamma72 = LinearToGammaSpace( mainTex78.rgb );
			float3 gammaToLinear74 = GammaToLinearSpace( ( float4( linearToGamma72 , 0.0 ) * i.vertexColor ).rgb );
			float4 lerpResult77 = lerp( float4( gammaToLinear74 , 0.0 ) , ( mainTex78 * i.vertexColor ) , 0.25);
			float4 finalAlbedo83 = ( lerpResult77 * unity_ColorSpaceDouble );
			float4 temp_output_84_0 = finalAlbedo83;
			o.Albedo = temp_output_84_0.rgb;
			o.Emission = temp_output_84_0.rgb;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows vertex:vertexDataFunc 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			AlphaToMask Off
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				float4 screenPos : TEXCOORD3;
				half4 color : COLOR0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v, customInputData );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				o.screenPos = ComputeScreenPos( o.pos );
				o.color = v.color;
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.screenPos = IN.screenPos;
				surfIN.vertexColor = IN.color;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				UnityGI gi;
				UNITY_INITIALIZE_OUTPUT( UnityGI, gi );
				o.Alpha = LightingStandardCustomLighting( o, worldViewDir, gi ).a;
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18301
1867;1031;1606;1000;2637.727;426.7597;1;True;False
Node;AmplifyShaderEditor.SimpleTimeNode;55;-2962.781,270.0351;Inherit;False;1;0;FLOAT;3;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;103;-84.96558,-272.0743;Inherit;False;GetBlueNoise;7;;25;3cbd807991120e84ab1d85f5cff4d57f;0;0;1;COLOR;0
Node;AmplifyShaderEditor.SimpleRemainderNode;56;-2760.344,241.8352;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0.3333333;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;34;134.3359,-368.231;Inherit;False;blueNoise;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;57;-2577.639,245.1435;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;12;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleRemainderNode;58;-2771.528,365.9198;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0.3333333;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;36;-2689.076,-132.9199;Inherit;False;34;blueNoise;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.FloorOpNode;60;-2420.227,247.7585;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;59;-2584.824,368.2282;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;6;False;1;FLOAT;0
Node;AmplifyShaderEditor.FloorOpNode;61;-2418.176,391.5739;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;62;-2291.176,250.5738;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;53;-2442.145,56.52612;Inherit;False;False;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.LerpOp;51;-2052.64,95.45042;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;63;-2299.176,405.5739;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SwizzleNode;54;-2431.235,148.2853;Inherit;False;FLOAT2;2;0;2;3;1;0;COLOR;0,0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.LerpOp;52;-1876.93,230.0095;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.TexturePropertyNode;33;-1859.664,-255.231;Inherit;True;Property;_MainTex;MainTex;1;0;Fetch;True;0;0;True;0;False;None;7a99efc7b04c92640895d9f192dc385f;False;white;Auto;Texture2D;-1;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.FunctionNode;65;-1544.664,-60.23102;Inherit;False;DitheredSampleUVs;-1;;19;1bae100dee9759348839076f4e1c8d64;0;3;18;SAMPLER2D;;False;20;FLOAT2;0,0;False;21;FLOAT2;0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.VertexColorNode;3;-1249,112;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;1;-1260,-91;Inherit;True;Property;_MainTex;MainTex;3;0;Create;True;0;0;False;0;False;-1;None;7a99efc7b04c92640895d9f192dc385f;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;81;-708.818,34.99457;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;23;-638.7656,378.9257;Inherit;False;Property;_VertexColourAffectsTransparency;Vertex Colour Affects Transparency;9;0;Create;True;0;0;False;1;ToggleUI;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;78;-888.714,-86.36285;Inherit;False;mainTex;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ComponentMaskNode;18;-459.5656,174.8257;Inherit;False;False;False;False;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;22;-205.7656,343.9257;Inherit;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;80;-1452.006,-774.7053;Inherit;False;78;mainTex;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;39;-173.1057,-21.87965;Inherit;False;Constant;_Float0;Float 0;9;0;Create;True;0;0;False;0;False;-0.003921569;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.VertexColorNode;82;-1451.427,-692.5945;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LinearToGammaNode;72;-1230.774,-793.1886;Inherit;False;0;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;25;78.49644,-1.217648;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;26;198.8455,18.66357;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;73;-1006.439,-780.9764;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;76;-982.3517,-527.8299;Inherit;False;Constant;_Float0;Float 0;3;0;Create;True;0;0;False;0;False;0.25;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;10;-1068,847;Inherit;False;Property;_EdgeFlutter;Edge Flutter;4;0;Create;True;0;0;False;0;False;0;3.26;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;16;-1077.766,1014.926;Inherit;False;Property;_SecondaryFactor;Secondary Factor;6;0;Create;True;0;0;False;0;False;0;0.91;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;9;-1067,765;Inherit;False;Property;_BranchPhase;Branch Phase;3;0;Create;True;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;75;-1012.352,-683.8298;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SqrtOpNode;27;355.8604,-9.359713;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;15;-1072.766,932.9257;Inherit;False;Property;_PrimaryFactor;Primary Factor;5;0;Create;True;0;0;False;0;False;0;0.12;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GammaToLinearNode;74;-860.9742,-791.7885;Inherit;False;0;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldPosInputsNode;95;-753.3406,653.6973;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;28;480.8455,-53.33643;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorSpaceDouble;5;-842.1647,-422.9153;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;17;-799.7656,846.9257;Inherit;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.LerpOp;77;-767.3519,-555.8298;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.CustomExpressionNode;88;-277.0834,789.2964;Inherit;False;// Setup to compensate for missing terrain data$waveAndDistance.x += _Time.x * waveAndDistance.w@$float4 _waveXSize = float4(0.012, 0.02, 0.06, 0.024) * waveAndDistance.y@$float4 _waveZSize = float4 (0.006, .02, 0.02, 0.05) * waveAndDistance.y@$float4 waveSpeed = float4 (0.3, .5, .4, 1.2) * 4@$float4 _waveXmove = float4(0.012, 0.02, -0.06, 0.048) * 2@$float4 _waveZmove = float4 (0.006, .02, -0.02, 0.1)@$float4 waves@$waves = vertex.x * _waveXSize@$waves += vertex.z * _waveZSize@$// Add in time to model them over time$waves += waveAndDistance.x * waveSpeed@$float4 s, c@$waves = frac (waves)@$FastSinCos (waves, s,c)@$s = s * s@$s = s * s@$s = s * waveAmount@$float3 waveMove = float3 (0,0,0)@$waveMove.x = dot (s, _waveXmove)@$waveMove.z = dot (s, _waveZmove)@$//vertex.xz -= waveMove.xz * waveAndDistance.z@$float3 final = vertex.xyz@$final.xz -= waveMove.xz * waveAndDistance.z@$//final -= vertex.xyz@$return final@;3;False;3;True;vertex;FLOAT4;0,0,0,0;In;;Inherit;False;True;waveAndDistance;FLOAT4;0,0,0,0;In;;Inherit;False;True;waveAmount;FLOAT;1;In;;Inherit;False;WaveGrass;True;False;0;3;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT;1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;29;543.8455,101.6636;Inherit;False;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;85;-559.7766,-553.8919;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CustomExpressionNode;68;80.52728,375.808;Inherit;False;return (x-cutout) / max(fwidth(x), 0.001) + cutout@;1;False;2;True;x;FLOAT;0;In;;Inherit;False;True;cutout;FLOAT;0.25;In;;Inherit;False;SharpenAlpha;True;False;0;2;0;FLOAT;0;False;1;FLOAT;0.25;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;66;-199.2562,250.1924;Inherit;False;Property;_SharpenAlpha;Sharpen Alpha;10;0;Create;True;0;0;False;1;ToggleUI;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;67;632.3851,296.5242;Inherit;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WorldToObjectTransfNode;96;4.659424,698.6973;Inherit;False;1;0;FLOAT4;0,0,0,1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;83;-419.5532,-560.4685;Inherit;False;finalAlbedo;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.PosVertexDataNode;97;33.06194,529.7408;Inherit;False;1;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SaturateNode;69;817.8679,337.2603;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CustomExpressionNode;86;-1669.068,-953.1001;Inherit;False;#define COLOR0 COLOR0_centroid;1;False;1;True;In0;FLOAT;0;In;;Inherit;False;VColFix;False;True;0;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;6;-465,595;Inherit;False;Terrain Wind Animate Vertex;-1;;20;3bc81bd4568a7094daabf2ccd6a7e125;0;3;2;FLOAT4;0,0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;84;746.3507,173.1601;Inherit;False;83;finalAlbedo;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.StaticSwitch;99;364.5055,521.9813;Inherit;False;Property;BLOOM;Wind Sway;2;0;Create;False;0;0;False;0;False;0;0;1;True;BLOOM;Toggle;2;_;BLOOM;Fetch;False;9;1;FLOAT4;0,0,0,0;False;0;FLOAT4;0,0,0,0;False;2;FLOAT4;0,0,0,0;False;3;FLOAT4;0,0,0,0;False;4;FLOAT4;0,0,0,0;False;5;FLOAT4;0,0,0,0;False;6;FLOAT4;0,0,0,0;False;7;FLOAT4;0,0,0,0;False;8;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.CustomExpressionNode;100;-2081.278,-52.96396;Inherit;False;int no = (_Time.y % (1.0/8.0))*32@$return noise[no]@;1;False;2;True;noise;FLOAT4;0,0,0,0;In;;Inherit;False;True;noise_TexelSize;FLOAT4;0,0,0,0;In;;Inherit;False;noiseSwitched;False;True;0;2;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;983.3999,155.3;Float;False;True;-1;2;ASEMaterialInspector;0;0;CustomLighting;PSU Cutout;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;TransparentCutout;;AlphaTest;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Absolute;0;;0;-1;-1;-1;0;True;0;0;False;-1;-1;0;False;-1;1;Pragma;shader_feature BLOOM;False;;Custom;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;56;0;55;0
WireConnection;34;0;103;0
WireConnection;57;0;56;0
WireConnection;58;0;55;0
WireConnection;60;0;57;0
WireConnection;59;0;58;0
WireConnection;61;0;59;0
WireConnection;62;0;60;0
WireConnection;53;0;36;0
WireConnection;51;0;36;0
WireConnection;51;1;53;0
WireConnection;51;2;62;0
WireConnection;63;0;61;0
WireConnection;54;0;36;0
WireConnection;52;0;51;0
WireConnection;52;1;54;0
WireConnection;52;2;63;0
WireConnection;65;18;33;0
WireConnection;65;20;52;0
WireConnection;1;1;65;0
WireConnection;81;0;1;4
WireConnection;81;1;3;4
WireConnection;78;0;1;0
WireConnection;18;0;81;0
WireConnection;22;0;23;0
WireConnection;22;2;18;0
WireConnection;22;3;1;4
WireConnection;22;4;1;4
WireConnection;72;0;80;0
WireConnection;25;0;39;0
WireConnection;25;1;22;0
WireConnection;26;0;25;0
WireConnection;73;0;72;0
WireConnection;73;1;82;0
WireConnection;75;0;80;0
WireConnection;75;1;82;0
WireConnection;27;0;26;0
WireConnection;74;0;73;0
WireConnection;28;0;103;0
WireConnection;28;1;27;0
WireConnection;17;0;9;0
WireConnection;17;1;10;0
WireConnection;17;2;15;0
WireConnection;17;3;16;0
WireConnection;77;0;74;0
WireConnection;77;1;75;0
WireConnection;77;2;76;0
WireConnection;88;0;95;0
WireConnection;88;1;17;0
WireConnection;29;0;22;0
WireConnection;29;1;28;0
WireConnection;85;0;77;0
WireConnection;85;1;5;0
WireConnection;68;0;22;0
WireConnection;67;0;66;0
WireConnection;67;2;68;0
WireConnection;67;3;29;0
WireConnection;67;4;29;0
WireConnection;96;0;88;0
WireConnection;83;0;85;0
WireConnection;69;0;67;0
WireConnection;6;4;17;0
WireConnection;99;1;97;0
WireConnection;99;0;96;0
WireConnection;100;0;36;0
WireConnection;0;0;84;0
WireConnection;0;2;84;0
WireConnection;0;9;69;0
WireConnection;0;11;99;0
ASEEND*/
//CHKSM=004B540AD85ABA71D6C7B051A596F821EFB50721