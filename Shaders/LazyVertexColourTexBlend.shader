Shader "Silent/LazyVertexColourTexBlend"
{
    Properties
    {
        _BlendSharp ("Blending Softness", Vector) = (1.0, 1.0, 1.0, 1.0)
        _BlendMask ("Blending Mask (R)", 2D) = "grey" {}
        _MaskStr("Blending Mask Power", Vector) = (1.0, 1.0, 1.0, 1.0)

        [Header(Base Map)]
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        [NoScaleOffset][Normal]_BumpMap ("Normal Map(XYZ)", 2D) = "bump" {}
        [NoScaleOffset]_MaskMap ("Mask (RGBA)", 2D) = "green" {}

        [Header(Vertex Color R)]
        _MainTexR ("Albedo (RGB)", 2D) = "white" {}
        [NoScaleOffset][Normal]_BumpMapR ("Normal Map(XYZ)", 2D) = "bump" {}
        [NoScaleOffset]_MaskMapR ("Mask (RGBA)", 2D) = "green" {}

        [Header(Vertex Color G)]
        _MainTexG ("Albedo (RGB)", 2D) = "white" {}
        [NoScaleOffset][Normal]_BumpMapG ("Normal Map(XYZ)", 2D) = "bump" {}
        [NoScaleOffset]_MaskMapG ("Mask (RGBA)", 2D) = "green" {}

        [Header(Vertex Color B)]
        _MainTexB ("Albedo (RGB)", 2D) = "white" {}
        [NoScaleOffset][Normal]_BumpMapB ("Normal Map(XYZ)", 2D) = "bump" {}
        [NoScaleOffset]_MaskMapB ("Mask (RGBA)", 2D) = "green" {}

        [Header(Vertex Color A)]
        _MainTexA ("Albedo (RGB)", 2D) = "white" {}
        [NoScaleOffset][Normal]_BumpMapA ("Normal Map(XYZ)", 2D) = "bump" {}
        [NoScaleOffset]_MaskMapA ("Mask (RGBA)", 2D) = "green" {}

        [Header(System)]
        [HideInInspector]__texcoord("texcoord", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        #pragma target 5.0

#if defined(SHADER_STAGE_VERTEX) || defined(SHADER_STAGE_FRAGMENT) || defined(SHADER_STAGE_DOMAIN) || defined(SHADER_STAGE_HULL) || defined(SHADER_STAGE_GEOMETRY)
#define TEX2DHALF Texture2D<half4>
#define TEXLOAD(tex, uvcoord) tex.Load(uvcoord)
#undef TRANSFORM_TEX
//#define TRANSFORM_TEX(a, b) ((a).xy)
#define TRANSFORM_TEX(tex,name) (tex.xy * name##_ST.xy + name##_ST.zw)
#else
#define precise
#define centroid
#define TEX2DHALF float4
#define TEXLOAD(tex, uvcoord) half4(1,0,1,1)
#endif

        sampler2D _MainTex;
        sampler2D _BumpMap;
        sampler2D _MaskMap;
        float4 _MainTex_ST;

        UNITY_DECLARE_TEX2D(_MainTexR); UNITY_DECLARE_TEX2D(_MainTexG); UNITY_DECLARE_TEX2D(_MainTexB); UNITY_DECLARE_TEX2D(_MainTexA);
        UNITY_DECLARE_TEX2D_NOSAMPLER(_BumpMapR); UNITY_DECLARE_TEX2D_NOSAMPLER(_BumpMapG); UNITY_DECLARE_TEX2D_NOSAMPLER(_BumpMapB); UNITY_DECLARE_TEX2D_NOSAMPLER(_BumpMapA);
        UNITY_DECLARE_TEX2D_NOSAMPLER(_MaskMapR); UNITY_DECLARE_TEX2D_NOSAMPLER(_MaskMapG); UNITY_DECLARE_TEX2D_NOSAMPLER(_MaskMapB); UNITY_DECLARE_TEX2D_NOSAMPLER(_MaskMapA);
        float4 _MainTexR_ST; float4 _MainTexG_ST; float4 _MainTexB_ST; float4 _MainTexA_ST;

        sampler2D _BlendMask;
        float4 _BlendMask_ST;
        float4 _BlendSharp;
        float4 _MaskStr;

        struct Input
        {
            float2 uv__texcoord;
            nointerpolation float4 color : COLOR_centroid;
        };

        // fixed4 _Color;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void addLayer(float weight, float2 uv, float4 uv_ST,
            sampler2D tex_A, inout float4 albedoAlpha, 
            sampler2D tex_N, inout float3 normal, 
            sampler2D tex_M, inout float4 mask)
        {
            if (weight > 0)
            {
                uv = uv * uv_ST.xy + uv_ST.zw;
                albedoAlpha += weight * UNITY_SAMPLE_TEX2D(tex_A, uv);
                normal += weight * UnpackScaleNormal(UNITY_SAMPLE_TEX2D_SAMPLER(tex_N, tex_A, uv), 1.0);
                mask += weight * UNITY_SAMPLE_TEX2D_SAMPLER(tex_M, tex_A, uv);
            }
        }

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Blend weights in vertex colours
            fixed4 weights = IN.color;
            float blendMod = tex2D(_BlendMask, IN.uv__texcoord * _BlendMask_ST.xy + _BlendMask_ST.zw);

            blendMod = (0.5 * blendMod - 0.5) * _MaskStr;

            float4 sharpPow = _BlendSharp * 0.5;
            weights = smoothstep(0.5-_BlendSharp, 0.5+_BlendSharp, weights+blendMod);
    
            weights = max(weights, 0);
            weights /= dot(weights, 1.0);
            float baseW = dot(weights, 1.0);

            float4 c = 0;
            float3 n = 0;
            float4 m = 0;

            addLayer(weights.r, IN.uv__texcoord, _MainTexR_ST, _MainTexR, c, _BumpMapR, n, _MaskMapR, m);
            addLayer(weights.g, IN.uv__texcoord, _MainTexG_ST, _MainTexG, c, _BumpMapG, n, _MaskMapG, m);
            addLayer(weights.b, IN.uv__texcoord, _MainTexB_ST, _MainTexB, c, _BumpMapB, n, _MaskMapB, m);
            addLayer(weights.a, IN.uv__texcoord, _MainTexA_ST, _MainTexA, c, _BumpMapA, n, _MaskMapA, m);

#if 0
            o.Albedo = weights;
#else
            o.Albedo = c.rgb;
            o.Normal = n.xyz;
            o.Metallic = m.r;
            o.Smoothness = m.a;
            o.Occlusion = m.g;
            o.Alpha = c.a;
#endif
        }
        ENDCG
    }
    FallBack "Standard"
}
