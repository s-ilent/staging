// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'
// Based on original by "LODDER".

#ifndef TERRAIN_SPLATMAP_CUSTOM_CGINC_INCLUDED
#define TERRAIN_SPLATMAP_CUSTOM_CGINC_INCLUDED

// Since 2018.3 we changed from _TERRAIN_NORMAL_MAP to _NORMALMAP to save 1 keyword.
// Since 2019.2 terrain keywords are changed to  local keywords so it doesn't really matter. You can use both.
#if defined(_NORMALMAP) && !defined(_TERRAIN_NORMAL_MAP)
    #define _TERRAIN_NORMAL_MAP
#elif !defined(_NORMALMAP) && defined(_TERRAIN_NORMAL_MAP)
    #define _NORMALMAP
#endif

struct Input
{
    float4 tc_Control;
	float3 worldPos;
	float3 vertNormal;
    #ifndef TERRAIN_BASE_PASS
        UNITY_FOG_COORDS(4) // needed because finalcolor oppresses fog code generation.
    #endif
};

sampler2D _Control;
float4 _Control_ST;
float4 _Control_TexelSize;

sampler2D _Splat0,_Splat1,_Splat2,_Splat3;
float4 _Splat0_ST, _Splat1_ST, _Splat2_ST, _Splat3_ST;

float _tiles0x; float _tiles0y; float _tiles0z; 
float _tiles1x; float _tiles1y; float _tiles1z; 
float _tiles2x; float _tiles2y; float _tiles2z; 
float _tiles3x; float _tiles3y; float _tiles3z; 
float _offset0x; float _offset0y; float _offset0z; 
float _offset1x; float _offset1y; float _offset1z; 
float _offset2x; float _offset2y; float _offset2z; 
float _offset3x; float _offset3y; float _offset3z; 

#if defined(UNITY_INSTANCING_ENABLED) && !defined(SHADER_API_D3D11_9X)
    sampler2D _TerrainHeightmapTexture;
    sampler2D _TerrainNormalmapTexture;
    float4    _TerrainHeightmapRecipSize;   // float4(1.0f/width, 1.0f/height, 1.0f/(width-1), 1.0f/(height-1))
    float4    _TerrainHeightmapScale;       // float4(hmScale.x, hmScale.y / (float)(kMaxHeight), hmScale.z, 0.0f)
#endif

UNITY_INSTANCING_BUFFER_START(Terrain)
    UNITY_DEFINE_INSTANCED_PROP(float4, _TerrainPatchInstanceData) // float4(xBase, yBase, skipScale, ~)
UNITY_INSTANCING_BUFFER_END(Terrain)

float4 _Color0;
float4 _Color1;
float4 _Color2;
float4 _Color3;

sampler2D _MainTex;

fixed3 normal;
float3 worldPos;

#ifdef _NORMALMAP
	sampler2D _Normal0, _Normal1, _Normal2, _Normal3;
    float _NormalScale0, _NormalScale1, _NormalScale2, _NormalScale3;
#endif

#ifdef _ALPHATEST_ON
    sampler2D _TerrainHolesTexture;

    void ClipHoles(float2 uv)
    {
        float hole = tex2D(_TerrainHolesTexture, uv).r;
        clip(hole == 0.0f ? -1 : 1);
    }
#endif

#if defined(TERRAIN_BASE_PASS) && defined(UNITY_PASS_META)
    // When we render albedo for GI baking, we actually need to take the ST
    float4 _MainTex_ST;
#endif

sampler2D _TexBlendNoise; float4 _TexBlendNoise_ST;
float _TexBlendNoisePower;
void SplatmapVert_Custom(inout appdata_full v, out Input data)
{
	UNITY_INITIALIZE_OUTPUT(Input, data);

#if defined(UNITY_INSTANCING_ENABLED) && !defined(SHADER_API_D3D11_9X)

    float2 patchVertex = v.vertex.xy;
    float4 instanceData = UNITY_ACCESS_INSTANCED_PROP(Terrain, _TerrainPatchInstanceData);

    float4 uvscale = instanceData.z * _TerrainHeightmapRecipSize;
    float4 uvoffset = instanceData.xyxy * uvscale;
    uvoffset.xy += 0.5f * _TerrainHeightmapRecipSize.xy;
    float2 sampleCoords = (patchVertex.xy * uvscale.xy + uvoffset.xy);

    float hm = UnpackHeightmap(tex2Dlod(_TerrainHeightmapTexture, float4(sampleCoords, 0, 0)));
    v.vertex.xz = (patchVertex.xy + instanceData.xy) * _TerrainHeightmapScale.xz * instanceData.z;  //(x + xBase) * hmScale.x * skipScale;
    v.vertex.y = hm * _TerrainHeightmapScale.y;
    v.vertex.w = 1.0f;

    v.texcoord.xy = (patchVertex.xy * uvscale.zw + uvoffset.zw);
    v.texcoord3 = v.texcoord2 = v.texcoord1 = v.texcoord;

    #ifdef TERRAIN_INSTANCED_PERPIXEL_NORMAL
        v.normal = float3(0, 1, 0); // TODO: reconstruct the tangent space in the pixel shader. Seems to be hard with surface shader especially when other attributes are packed together with tSpace.
        data.tc_Control.zw = sampleCoords;
    #else
        float3 nor = tex2Dlod(_TerrainNormalmapTexture, float4(sampleCoords, 0, 0)).xyz;
        v.normal = 2.0f * nor - 1.0f;
    #endif
#endif

	v.tangent.xyz = cross(v.normal, float3(0,0,1));
	v.tangent.w = -1;
	data.vertNormal = v.normal;

	//data.tc_Control.xy = TRANSFORM_TEX(v.texcoord, _Control);	// Need to manually transform uv here, as we choose not to use 'uv' prefix for this texcoord.
	data.tc_Control.xy = v.texcoord.xy;

#ifdef TERRAIN_BASE_PASS
    #ifdef UNITY_PASS_META
        data.tc_Control.xy = TRANSFORM_TEX(v.texcoord.xy, _MainTex);
    #endif
#else
    float4 pos = UnityObjectToClipPos(v.vertex);
    UNITY_TRANSFER_FOG(data, pos);
#endif

}

float4 smootherstep(float4 edge0, float4 edge1, float4 x) {
  x = saturate((x - edge0) / (edge1 - edge0));
  return x * x * x * (x * (x * 6 - 15) + 10);
}

float4 gain(float4 x, float4 k) 
{
    float4 a = 0.5*pow(2.0*((x<0.5)?x:1.0-x), k);
    return (x<0.5)?a:1.0-a;
}

float expStep( float x, float k, float n )
{
    return exp( -k*pow(x,n) );
}

//GeometricSpecularAA (Valve Method)
float GeometricAASpecular(float3 normal, float smoothness){
    float3 vNormalWsDdx = ddx(normal);
    float3 vNormalWsDdy = ddy(normal);
    float flGeometricRoughnessFactor = pow(saturate(max(dot(vNormalWsDdx, vNormalWsDdx), dot(vNormalWsDdy, vNormalWsDdy))), 0.333);
    return min(smoothness, 1.0 - flGeometricRoughnessFactor);
}

float cubic2 (float x)
{
	return x * x * x * (x * (6 * x - 15) + 10);
}

// https://iquilezles.org/www/articles/biplanar/biplanar.htm
// "p" point being textured
// "n" surface normal at "p"
// "k" controls the sharpness of the blending in the transitions areas
// "s" texture sampler
float4 biplanar( sampler2D sam, float3 p, float3 n, float k )
{
    // grab coord derivatives for texturing
    float3 dpdx = ddx(p);
    float3 dpdy = ddy(p);
    n = abs(n);

    // determine major axis (in x; yz are following axis)
    int3 ma =  (n.x>n.y && n.x>n.z) ? int3(0,1,2) :
               (n.y>n.z)            ? int3(1,2,0) :
                                      int3(2,0,1) ;
    // determine minor axis (in x; yz are following axis)
    int3 mi =  (n.x<n.y && n.x<n.z) ? int3(0,1,2) :
               (n.y<n.z)            ? int3(1,2,0) :
                                      int3(2,0,1) ;
    // determine median axis (in x;  yz are following axis)
    int3 me = clamp(3 - mi - ma, 0, 2); 
    
    // project+fetch
    float4 x = tex2Dgrad( sam, float2(   p[ma.y],   p[ma.z]), 
                               float2(dpdx[ma.y],dpdx[ma.z]), 
                               float2(dpdy[ma.y],dpdy[ma.z]) );
    float4 y = tex2Dgrad( sam, float2(   p[me.y],   p[me.z]), 
                               float2(dpdx[me.y],dpdx[me.z]),
                               float2(dpdy[me.y],dpdy[me.z]) );
    
    // blend factors
    float2 w = float2(n[ma.x],n[me.x]);
    // make local support
    w = clamp( (w-0.5773)/(1.0-0.5773), 0.0, 1.0 );
    // shape transition
    w = pow( w, k/8.0 );
    // blend and return
    return (x*w.x + y*w.y) / (w.x + w.y);
}


float hash(float2 p){
    p  = frac(p * .1031);
    p += dot(p, p.yx + 19.19);
    return frac((p.x + p.y) * p.x);
}

float4 normalNoise(float2 x) {
    float2 p = floor(x);
    float2 f = frac(x);

    float a = hash(p);
    float b = hash(p + float2(1., 0.));
    float c = hash(p + float2(0., 1.));
    float d = hash(p + float2(1., 1.));
    
    float2 g = f - 1.;
    g *= g;
    
    float2 v = 1. - g;
    v *= v;
    
    float noise = lerp(lerp(a, b, v.x), lerp(c, d, v.x), v.y);
    
    float2 z = 30.*g*f*f;
    
    float2 derivatives = z * (float2(b, c) + (a - b - c + d) * v.yx - a);
    
    float3 normal = cross(float3(1,0,derivatives.x),float3(0,1,derivatives.y));

    return float4(normal, noise);
}        

float4 normalNoise2 (float2 x) {
    x*=12.0/0.05;
    float4 noise = normalNoise(x);
    x = mul(x, float2x2(2.0,1.2,-1.2,2.0));
    noise+=normalNoise(x)*0.5;
    x = mul(x, float2x2(2.0,1.2,-1.2,2.0));
    noise+=normalNoise(x)*0.25;
    x = mul(x, float2x2(2.0,1.2,-1.2,2.0));
    noise+=normalNoise(x)*0.125;
    x = mul(x, float2x2(2.0,1.2,-1.2,2.0));
    noise+=normalNoise(x)*.0625;

    noise.xyz=normalize(noise.xyz);
    return noise;
}

#ifdef TERRAIN_STANDARD_SHADER
void SplatmapMix(Input IN, half4 defaultAlpha, out half4 splat_control, out half weight, out fixed4 mixedDiffuse, inout fixed3 mixedNormal)
#else
void SplatmapMix(Input IN, out half4 splat_control, out half weight, out fixed4 mixedDiffuse, inout fixed3 mixedNormal)
#endif
{
    #ifdef _ALPHATEST_ON
        ClipHoles(IN.tc_Control.xy);
    #endif

#if 0
	float3 blendMod1 = tex2D(_TexBlendNoise, TRANSFORM_TEX(IN.tc_Control, _TexBlendNoise));
	// Blendmod using tc_control UVs, which might be a bad idea. We'll see.
	float3 blendMod2 = tex2D(_TexBlendNoise, 2*TRANSFORM_TEX(IN.tc_Control, _TexBlendNoise));

	float3 bm = blendMod1+(blendMod2*2-1);
	//bm = (saturate(bm))*2-1;

    // adjust splatUVs so the edges of the terrain tile lie on pixel centers
    float2 splatUV = (IN.tc_Control.xy * (_Control_TexelSize.zw - 1.0f) + 0.5f + bm) * _Control_TexelSize.xy;
	splat_control = tex2D(_Control, splatUV);

    #if !defined(SHADER_API_MOBILE) && defined(TERRAIN_SPLAT_ADDPASS)
        clip(weight == 0.0f ? -1 : 1);
    #endif

    // Sharpen blend areas 
	splat_control = gain(splat_control, blendMod1.xxxx);
	splat_control = gain(splat_control, blendMod2.zzzz);
	splat_control = smootherstep(0.5-_TexBlendNoisePower, 0.5+_TexBlendNoisePower, splat_control);
#else
    float4 normalNoise = normalNoise2(IN.tc_Control);
    // adjust splatUVs so the edges of the terrain tile lie on pixel centers
    float2 splatUV = (IN.tc_Control.xy * (_Control_TexelSize.zw - 1.0f) + 0.5f) * _Control_TexelSize.xy;
    splatUV += normalNoise * 0.0025;
	splat_control = tex2D(_Control, splatUV);

    #if !defined(SHADER_API_MOBILE) && defined(TERRAIN_SPLAT_ADDPASS)
        clip(weight == 0.0f ? -1 : 1);
    #endif

    // Sharpen blend areas 
	splat_control = gain(splat_control, normalNoise.xxxx * 0.5 + 0.5);
	_TexBlendNoisePower *= 10;
	//splat_control = gain(splat_control, normalNoise.zzzz * 0.000001);
	splat_control = smootherstep(0.5-_TexBlendNoisePower, 0.5+_TexBlendNoisePower, splat_control);
#endif

	weight = dot(splat_control, half4(1,1,1,1));
	#if !defined(SHADER_API_MOBILE) && defined(TERRAIN_SPLAT_ADDPASS)
		clip(weight - 0.0039 /*1/255*/);
	#endif

	worldPos = IN.worldPos;

	#ifdef _TERRAIN_NORMAL_MAP
		normal = abs(IN.vertNormal);
	#else
		normal = abs(mixedNormal);
	#endif
	// Normalize weights before lighting and restore weights in final modifier functions so that the overal
	// lighting result can be correctly weighted.
	splat_control /= (weight + 1e-3f);

	mixedDiffuse = 0.0f;
	fixed3 nrm = 0.0f;

	// Transfer smoothness
	#ifdef TERRAIN_STANDARD_SHADER
	_Color0.a = defaultAlpha.r;
	_Color1.a = defaultAlpha.g;
	_Color2.a = defaultAlpha.b;
	_Color3.a = defaultAlpha.a;
	#endif

/*
	float3 tighten = 0.576;
	float3 absVertexNormal = abs(normalize(normal));
	float3 weights = absVertexNormal - tighten;

	weights *= 3;

	float y = normal.y;
	float z = normal.z;
	float2 y0 = IN.worldPos.zy * _tiles0x + _offset0x;
	float2 x0 = IN.worldPos.xz * _tiles0z + _offset0z;
	float2 z0 = IN.worldPos.xy * _tiles0y + _offset0y;
	
	float2 y1 = IN.worldPos.zy * _tiles1x + _offset1x;
	float2 x1 = IN.worldPos.xz * _tiles1z + _offset1z;
	float2 z1 = IN.worldPos.xy * _tiles1y + _offset1y;
	
	float2 y2 = IN.worldPos.zy * _tiles2x + _offset2x;
	float2 x2 = IN.worldPos.xz * _tiles2z + _offset2z;
	float2 z2 = IN.worldPos.xy * _tiles2y + _offset2y;
	
	float2 y3 = IN.worldPos.zy * _tiles3x + _offset3x;
	float2 x3 = IN.worldPos.xz * _tiles3z + _offset3z;
	float2 z3 = IN.worldPos.xy * _tiles3y + _offset3y;

	// Landscale creation and rendering in REDengine 3
	// "Prefer branching to texture sampling"

	if(weights.x > 0) mixedDiffuse += weights.x * tex2D(_Splat0, y0) * splat_control.r * _Color0;
	if(weights.x > 0) mixedDiffuse += weights.x * tex2D(_Splat1, y1) * splat_control.g * _Color1;
	if(weights.x > 0) mixedDiffuse += weights.x * tex2D(_Splat2, y2) * splat_control.b * _Color2;
	if(weights.x > 0) mixedDiffuse += weights.x * tex2D(_Splat3, y3) * splat_control.a * _Color3;

	#ifdef _TERRAIN_NORMAL_MAP
		if(weights.x > 0) nrm += weights.x * UnpackNormal(tex2D(_Normal0, y0)) * splat_control.r;
		if(weights.x > 0) nrm += weights.x * UnpackNormal(tex2D(_Normal1, y1)) * splat_control.g;
		if(weights.x > 0) nrm += weights.x * UnpackNormal(tex2D(_Normal2, y2)) * splat_control.b;
		if(weights.x > 0) nrm += weights.x * UnpackNormal(tex2D(_Normal3, y3)) * splat_control.a;
	#endif

	if(weights.y > 0) mixedDiffuse += weights.y * tex2D(_Splat0, x0) * splat_control.r * _Color0;
	if(weights.y > 0) mixedDiffuse += weights.y * tex2D(_Splat1, x1) * splat_control.g * _Color1;
	if(weights.y > 0) mixedDiffuse += weights.y * tex2D(_Splat2, x2) * splat_control.b * _Color2;
	if(weights.y > 0) mixedDiffuse += weights.y * tex2D(_Splat3, x3) * splat_control.a * _Color3;

	#ifdef _TERRAIN_NORMAL_MAP
		if(weights.y > 0) nrm += weights.y * UnpackNormal(tex2D(_Normal0, x0)) * splat_control.r;
		if(weights.y > 0) nrm += weights.y * UnpackNormal(tex2D(_Normal1, x1)) * splat_control.g;
		if(weights.y > 0) nrm += weights.y * UnpackNormal(tex2D(_Normal2, x2)) * splat_control.b;
		if(weights.y > 0) nrm += weights.y * UnpackNormal(tex2D(_Normal3, x3)) * splat_control.a;
	#endif

	if(weights.z > 0) mixedDiffuse += weights.z * tex2D(_Splat0, z0) * splat_control.r * _Color0;
	if(weights.z > 0) mixedDiffuse += weights.z * tex2D(_Splat1, z1) * splat_control.g * _Color1;
	if(weights.z > 0) mixedDiffuse += weights.z * tex2D(_Splat2, z2) * splat_control.b * _Color2;
	if(weights.z > 0) mixedDiffuse += weights.z * tex2D(_Splat3, z3) * splat_control.a * _Color3;

	#ifdef _TERRAIN_NORMAL_MAP
		if(weights.z > 0) nrm += weights.z * UnpackNormal(tex2D(_Normal0, z0)) * splat_control.r;
		if(weights.z > 0) nrm += weights.z * UnpackNormal(tex2D(_Normal1, z1)) * splat_control.g;
		if(weights.z > 0) nrm += weights.z * UnpackNormal(tex2D(_Normal2, z2)) * splat_control.b;
		if(weights.z > 0) nrm += weights.z * UnpackNormal(tex2D(_Normal3, z3)) * splat_control.a;
	#endif
		
	#ifdef _TERRAIN_NORMAL_MAP
        nrm.z += 1e-5f; // to avoid nan after normalizing
		mixedNormal = normalize(nrm);
	#endif
*/

// "p" point being textured
// "n" surface normal at "p"
// "k" controls the sharpness of the blending in the transitions areas
// "s" texture sampler

	float3 splat0Pos = IN.worldPos * float3(_tiles0x, _tiles0y, _tiles0z);
	if(splat_control[0] > 0) mixedDiffuse += biplanar( _Splat0, splat0Pos, normal, 1.0) * splat_control[0] * _Color0;
	#ifdef _TERRAIN_NORMAL_MAP
	if(splat_control[0] > 0) nrm += UnpackNormal(biplanar( _Normal0, splat0Pos, normal, 1.0)) * splat_control[0];
	#endif

	float3 splat1Pos = IN.worldPos * float3(_tiles1x, _tiles1y, _tiles1z);
	if(splat_control[1] > 0) mixedDiffuse += biplanar( _Splat1, splat1Pos, normal, 1.0) * splat_control[1] * _Color0;
	#ifdef _TERRAIN_NORMAL_MAP
	if(splat_control[1] > 0) nrm += UnpackNormal(biplanar( _Normal1, splat1Pos, normal, 1.0)) * splat_control[1];
	#endif

	float3 splat2Pos = IN.worldPos * float3(_tiles2x, _tiles2y, _tiles2z);
	if(splat_control[2] > 0) mixedDiffuse += biplanar( _Splat2, splat2Pos, normal, 1.0) * splat_control[2] * _Color0;
	#ifdef _TERRAIN_NORMAL_MAP
	if(splat_control[2] > 0) nrm += UnpackNormal(biplanar( _Normal2, splat2Pos, normal, 1.0)) * splat_control[2];
	#endif

	float3 splat3Pos = IN.worldPos * float3(_tiles3x, _tiles3y, _tiles3z);
	if(splat_control[3] > 0) mixedDiffuse += biplanar( _Splat3, splat3Pos, normal, 1.0) * splat_control[3] * _Color0;
	#ifdef _TERRAIN_NORMAL_MAP
	if(splat_control[3] > 0) nrm += UnpackNormal(biplanar( _Normal3, splat3Pos, normal, 1.0)) * splat_control[3];
	#endif
		
	#ifdef _TERRAIN_NORMAL_MAP
        nrm.z += 1e-5f; // to avoid nan after normalizing
		mixedNormal = normalize(nrm);
	#endif

    #if defined(UNITY_INSTANCING_ENABLED) && !defined(SHADER_API_D3D11_9X) && defined(TERRAIN_INSTANCED_PERPIXEL_NORMAL)
        float3 geomNormal = normalize(tex2D(_TerrainNormalmapTexture, IN.tc_Control.zw).xyz * 2 - 1);
        #ifdef _NORMALMAP
            float3 geomTangent = normalize(cross(geomNormal, float3(0, 0, 1)));
            float3 geomBitangent = normalize(cross(geomTangent, geomNormal));
            mixedNormal = mixedNormal.x * geomTangent
                          + mixedNormal.y * geomBitangent
                          + mixedNormal.z * geomNormal;
        #else
            mixedNormal = geomNormal;
        #endif
        mixedNormal = mixedNormal.xzy;
    #endif
}

#ifndef TERRAIN_SURFACE_OUTPUT
	#define TERRAIN_SURFACE_OUTPUT SurfaceOutput
#endif

void SplatmapFinalColor(Input IN, TERRAIN_SURFACE_OUTPUT o, inout fixed4 color)
{
	color *= o.Alpha;
	#ifdef TERRAIN_SPLAT_ADDPASS
		UNITY_APPLY_FOG_COLOR(IN.fogCoord, color, fixed4(0,0,0,0));
	#else
		UNITY_APPLY_FOG(IN.fogCoord, color);
	#endif
}

void SplatmapFinalPrepass(Input IN, TERRAIN_SURFACE_OUTPUT o, inout fixed4 normalSpec)
{
	normalSpec *= o.Alpha;
}

void SplatmapFinalGBuffer(Input IN, TERRAIN_SURFACE_OUTPUT o, inout half4 diffuse, inout half4 specSmoothness, inout half4 normal, inout half4 emission)
{
	diffuse.rgb *= o.Alpha;
	specSmoothness *= o.Alpha;
	normal.rgb *= o.Alpha;
	emission *= o.Alpha;
}

#endif // TERRAIN_SPLATMAP_COMMON_CGINC_INCLUDED