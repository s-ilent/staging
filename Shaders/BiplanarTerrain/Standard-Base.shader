// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "Nature/Terrain/Splatmap/Standard-Base Edit" {
    Properties {
        _MainTex ("Base (RGB) Smoothness (A)", 2D) = "white" {}
        _MetallicTex ("Metallic (R)", 2D) = "white" {}
        _DetailTex ("Detail", 2D) = "bump" {}
        _DetailScale ("Detail Scale", Float) = 1

        // used in fallback on old cards
        _Color ("Main Color", Color) = (1,1,1,1)
    }

    SubShader {
        Tags {
            "RenderType" = "Opaque"
            "Queue" = "Geometry-100"
        }
        LOD 200

        CGPROGRAM
        #pragma surface surf Standard vertex:SplatmapVert addshadow fullforwardshadows
        #pragma instancing_options assumeuniformscaling nomatrices nolightprobe nolightmap forwardadd
        #pragma target 3.0
        // needs more than 8 texcoords
        #pragma exclude_renderers gles

        #define TERRAIN_BASE_PASS
        #define TERRAIN_INSTANCED_PERPIXEL_NORMAL
        #include "TerrainSplatmapCommon.cginc"
        #include "UnityPBSLighting.cginc"

        sampler2D _MainTex;
        sampler2D _MetallicTex;
        sampler2D _DetailTex; float _DetailScale;


float hash(float2 p){
    p  = frac(p * .1031);
    p += dot(p, p.yx + 19.19);
    return frac((p.x + p.y) * p.x);
}

float4 normalNoise(float2 x) {
    float2 p = floor(x);
    float2 f = frac(x);

    float a = hash(p);
    float b = hash(p + float2(1., 0.));
    float c = hash(p + float2(0., 1.));
    float d = hash(p + float2(1., 1.));
    
    float2 g = f - 1.;
    g *= g;
    
    float2 v = 1. - g;
    v *= v;
    
    float noise = lerp(lerp(a, b, v.x), lerp(c, d, v.x), v.y);
    
    float2 z = 30.*g*f*f;
    
    float2 derivatives = z * (float2(b, c) + (a - b - c + d) * v.yx - a);
    
    float3 normal = cross(float3(1,0,derivatives.x),float3(0,1,derivatives.y));

    return float4(normal, noise);
}        

float4 normalNoise2 (float2 x) {
    x*=12.0/0.05;
    float4 noise = normalNoise(x);
    x = mul(x, float2x2(2.0,1.2,-1.2,2.0));
    noise+=normalNoise(x)*0.5;
    x = mul(x, float2x2(2.0,1.2,-1.2,2.0));
    noise+=normalNoise(x)*0.25;
    x = mul(x, float2x2(2.0,1.2,-1.2,2.0));
    noise+=normalNoise(x)*0.125;
    x = mul(x, float2x2(2.0,1.2,-1.2,2.0));
    noise+=normalNoise(x)*.0625;

    noise.xyz=normalize(noise.xyz);
    return noise;
}

//GeometricSpecularAA (Valve Method)
float GeometricAASpecular(float3 normal, float smoothness){
    float3 vNormalWsDdx = ddx(normal);
    float3 vNormalWsDdy = ddy(normal);
    float flGeometricRoughnessFactor = pow(saturate(max(dot(vNormalWsDdx, vNormalWsDdx), dot(vNormalWsDdy, vNormalWsDdy))), 0.333);
    return min(smoothness, 1.0 - flGeometricRoughnessFactor);
}

float3 blend_rnm(float4 n1, float4 n2)
{
    float3 t = n1.xyz*float3( 2,  2, 2) + float3(-1, -1,  0);
    float3 u = n2.xyz*float3(-2, -2, 2) + float3( 1,  1, -1);
    float3 r = t*dot(t, u) - u*t.z;
    return normalize(r);
}

float3 blend_pd(float4 n1, float4 n2)
{
    n1 = n1*2 - 1;
    n2 = n2.xyzz*float4(2, 2, 2, 0) + float4(-1, -1, -1, 0);
    float3 r = n1.xyz*n2.z + n2.xyw*n1.z;
    return normalize(r);
}

float3 blend_whiteout(float4 n1, float4 n2)
{
    n1 = n1*2 - 1;
    n2 = n2*2 - 1;
    float3 r = float3(n1.xy + n2.xy, n1.z*n2.z);
    return normalize(r);
}

        void surf (Input IN, inout SurfaceOutputStandard o) {
/*
            // Needed to get proper normals
            float3 worldInterpolatedNormalVector = WorldNormalVector ( IN, float3( 0, 0, 1 ) );

            // Triplanar map detail
            // Can't use this for triplanar mapping because we can't load new textures in VRchat.
            // But it makes distant terrain match the real terrain.
            float3 tighten = 0.576;
            float3 absVertexNormal = abs(normalize(worldInterpolatedNormalVector));
            float3 weights = absVertexNormal - tighten;
            weights *= 3;
            float fakeShade = (max(weights.x, max(weights.y, weights.z)));
*/
/*
            // Doesn't work.
            fixed4 nrm = 0.0f;
            _DetailScale = 0.05;
            if(weights.x > 0) nrm += weights.x * (tex2D(_DetailTex, IN.worldPos.zy*_DetailScale));
            if(weights.y > 0) nrm += weights.y * (tex2D(_DetailTex, IN.worldPos.xz*_DetailScale));
            if(weights.z > 0) nrm += weights.z * (tex2D(_DetailTex, IN.worldPos.xy*_DetailScale));
            o.Normal = normalize(nrm.xyz);
            //o.Albedo = (tex2Dlod(_DetailTex, float4(IN.tc.xy*512,0,0)));
*/


            float4 normalNoise = normalNoise2(IN.tc);
            half4 c = tex2D (_MainTex, IN.tc + normalNoise * 0.0025);

            // Fake detail
            //c = c - (fakeShade*normalNoise.a - 0.7)*0.02;
            c -= Luminance(tex2Dlod (_MainTex, float4(IN.tc.xy, 0, 3))) * 0.2 - 0.01;
            //c *= saturate(fakeShade+0.15);
            c *= 1.15;
            o.Albedo = c.rgb;

            #if defined(INSTANCING_ON) && defined(SHADER_TARGET_SURFACE_ANALYSIS) && defined(TERRAIN_INSTANCED_PERPIXEL_NORMAL)
                o.Normal = float3(0, 0, 1); // make sure that surface shader compiler realizes we write to normal, as UNITY_INSTANCING_ENABLED is not defined for SHADER_TARGET_SURFACE_ANALYSIS.
            #endif

            #if defined(UNITY_INSTANCING_ENABLED) && !defined(SHADER_API_D3D11_9X) && defined(TERRAIN_INSTANCED_PERPIXEL_NORMAL)
                o.Normal = normalize(tex2D(_TerrainNormalmapTexture, IN.tc.zw).xyz * 2 - 1).xzy;
                //o.Normal = (normalNoise.xyz * float3(0.5, 0.5, 1));
                //float4 baseNormal = normalize(tex2D(_TerrainNormalmapTexture, IN.tc.zw).xyzw * 2 - 1).xzyw;
                //o.Normal.xy = blend_whiteout(baseNormal, (normalNoise*2-1)*0.5);
            #endif

            o.Alpha = 1;
            // Crush smoothness or it'll be too shiny. 
            o.Smoothness = GeometricAASpecular(o.Normal, c.a*c.a);  
            o.Metallic = tex2D (_MetallicTex, IN.tc).r;
        }

        ENDCG

        UsePass "Hidden/Nature/Terrain/Utilities/PICKING"
        UsePass "Hidden/Nature/Terrain/Utilities/SELECTION"
    }

    FallBack "Diffuse"
}
