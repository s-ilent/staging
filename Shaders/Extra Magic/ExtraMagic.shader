// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Silent/Extra Magic"
{
	Properties
	{
		_Tint("Tint", Color) = (1,1,1,1)
		_Intensity("Intensity", Float) = 2
		_Noise1("Noise 1", 2D) = "white" {}
		_Noise1Speed("Noise 1 Speed", Float) = 0.1
		_Noise1Step("Noise 1 Step", Float) = 0.2
		_Noise1GateWidth("Noise 1 Gate Width", Float) = 0.1
		_Noise1Scale("Noise 1 Inversion Scale", Float) = 1
		_Noise2("Noise 2", 2D) = "white" {}
		_Noise2Speed("Noise 2 Speed", Float) = 0.1
		_Noise2Step("Noise 2 Step", Float) = 0.2
		_Noise2GateWidth("Noise 2 Gate Width", Float) = 0.1
		_Noise2Scale("Noise 2 Inversion Scale", Float) = 1
		_WarpNoise("Warp Noise", 2D) = "white" {}
		_WarpNoise2("Warp Noise 2", 2D) = "white" {}
		_WarpScale("Warp Scale", Range( -1 , 1)) = 0.1
		_WarpScale2("Warp Scale 2", Range( -1 , 1)) = 0.1
		[Enum(UnityEngine.Rendering.CullMode)]_CullMode("Cull Mode", Float) = 0
		[Enum(UnityEngine.Rendering.BlendMode)]_ParticleBlendSrc("Source Blend", Float) = 1
		[Enum(UnityEngine.Rendering.BlendMode)]_ParticleBlendDst("Destination Blend", Float) = 10
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull [_CullMode]
		ZWrite Off
		Blend [_ParticleBlendSrc] [_ParticleBlendDst]
		
		CGINCLUDE
		#include "UnityShaderVariables.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#if defined(SHADER_API_D3D11) || defined(SHADER_API_XBOXONE) || defined(UNITY_COMPILER_HLSLCC) || defined(SHADER_API_PSSL) || (defined(SHADER_TARGET_SURFACE_ANALYSIS) && !defined(SHADER_TARGET_SURFACE_ANALYSIS_MOJOSHADER))//ASE Sampler Macros
		#define SAMPLE_TEXTURE2D(tex,samplerTex,coord) tex.Sample(samplerTex,coord)
		#else//ASE Sampling Macros
		#define SAMPLE_TEXTURE2D(tex,samplerTex,coord) tex2D(tex,coord)
		#endif//ASE Sampling Macros

		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float _ParticleBlendSrc;
		uniform float _CullMode;
		uniform float _ParticleBlendDst;
		uniform float4 _Tint;
		uniform float _Intensity;
		uniform float _Noise1Step;
		uniform float _Noise1GateWidth;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_Noise1);
		uniform float4 _Noise1_ST;
		uniform float _Noise1Speed;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_WarpNoise);
		uniform float4 _WarpNoise_ST;
		SamplerState sampler_WarpNoise;
		uniform float _WarpScale;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_WarpNoise2);
		uniform float4 _WarpNoise2_ST;
		SamplerState sampler_WarpNoise2;
		uniform float _WarpScale2;
		SamplerState sampler_Noise1;
		uniform float _Noise1Scale;
		uniform float _Noise2Step;
		uniform float _Noise2GateWidth;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_Noise2);
		uniform float4 _Noise2_ST;
		uniform float _Noise2Speed;
		SamplerState sampler_Noise2;
		uniform float _Noise2Scale;

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float2 uv_TexCoord17 = i.uv_texcoord * _WarpNoise_ST.xy + ( _WarpNoise_ST.zw * _Time.y );
			float2 uv_TexCoord47 = i.uv_texcoord * _WarpNoise2_ST.xy + ( _WarpNoise2_ST.zw * _Time.y );
			float4 warpBase21 = ( ( SAMPLE_TEXTURE2D( _WarpNoise, sampler_WarpNoise, uv_TexCoord17 ) * _WarpScale ) + ( SAMPLE_TEXTURE2D( _WarpNoise2, sampler_WarpNoise2, uv_TexCoord47 ) * _WarpScale2 ) );
			float2 uv_TexCoord7 = i.uv_texcoord * _Noise1_ST.xy + frac( ( float4( ( _Noise1_ST.zw * _Time.y * _Noise1Speed ), 0.0 , 0.0 ) + warpBase21 ) ).rg;
			float4 tex2DNode1 = SAMPLE_TEXTURE2D( _Noise1, sampler_Noise1, uv_TexCoord7 );
			float smoothstepResult2 = smoothstep( _Noise1Step , ( _Noise1Step + _Noise1GateWidth ) , tex2DNode1.r);
			float2 uv_TexCoord32 = i.uv_texcoord * _Noise2_ST.xy + frac( ( float4( ( _Noise2_ST.zw * _Time.y * _Noise2Speed ), 0.0 , 0.0 ) + warpBase21 ) ).rg;
			float4 tex2DNode37 = SAMPLE_TEXTURE2D( _Noise2, sampler_Noise2, uv_TexCoord32 );
			float smoothstepResult34 = smoothstep( _Noise2Step , ( _Noise2Step + _Noise2GateWidth ) , tex2DNode37.r);
			float temp_output_39_0 = saturate( ( saturate( ( smoothstepResult2 - ( tex2DNode1.r * _Noise1Scale ) ) ) + saturate( ( smoothstepResult34 - ( tex2DNode37.r * _Noise2Scale ) ) ) ) );
			o.Emission = ( _Tint * _Intensity * temp_output_39_0 ).rgb;
			o.Alpha = ( temp_output_39_0 * _Tint.a );
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Unlit keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				SurfaceOutput o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutput, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18707
1948;302;1770;1686;1221.073;2147.513;1.870956;True;False
Node;AmplifyShaderEditor.TimeNode;15;-3042.825,1406.522;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TimeNode;44;-3069.905,1799.302;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureTransformNode;45;-3086.896,1644.604;Inherit;False;48;False;1;0;SAMPLER2D;;False;2;FLOAT2;0;FLOAT2;1
Node;AmplifyShaderEditor.TextureTransformNode;14;-3059.816,1251.824;Inherit;False;11;False;1;0;SAMPLER2D;;False;2;FLOAT2;0;FLOAT2;1
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;16;-2806.825,1430.522;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;46;-2833.905,1823.302;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;17;-2616.841,1356.349;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;47;-2643.921,1749.129;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;49;-2349.408,1977.678;Inherit;False;Property;_WarpScale2;Warp Scale 2;16;0;Create;True;0;0;False;0;False;0.1;0.03;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;48;-2371.741,1732.331;Inherit;True;Property;_WarpNoise2;Warp Noise 2;14;0;Create;True;0;0;False;0;False;-1;3c5f973b007707340b4e0287f21d4469;3c5f973b007707340b4e0287f21d4469;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;11;-2344.661,1339.551;Inherit;True;Property;_WarpNoise;Warp Noise;13;0;Create;True;0;0;False;0;False;-1;3c5f973b007707340b4e0287f21d4469;13082190cd61b4ae6bd3f9af876a29f4;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;20;-2322.328,1584.898;Inherit;False;Property;_WarpScale;Warp Scale;15;0;Create;True;0;0;False;0;False;0.1;0.03;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;19;-1977.328,1437.898;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;50;-2004.408,1830.678;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;51;-1765.726,1621.536;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TimeNode;26;-2480.237,450.73;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;40;-2474.186,129.9728;Inherit;False;Property;_Noise1Speed;Noise 1 Speed;4;0;Create;True;0;0;False;0;False;0.1;0.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureTransformNode;25;-2483.237,321.73;Inherit;False;37;False;1;0;SAMPLER2D;;False;2;FLOAT2;0;FLOAT2;1
Node;AmplifyShaderEditor.TextureTransformNode;10;-2439,-133.5;Inherit;False;1;False;1;0;SAMPLER2D;;False;2;FLOAT2;0;FLOAT2;1
Node;AmplifyShaderEditor.TimeNode;6;-2492.8,-21.9;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;21;-1615.456,1620.027;Inherit;False;warpBase;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;41;-2449.491,605.3884;Inherit;False;Property;_Noise2Speed;Noise 2 Speed;9;0;Create;True;0;0;False;0;False;0.1;0.15;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;28;-2244.237,474.73;Inherit;False;3;3;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;27;-2242.382,595.7252;Inherit;False;21;warpBase;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;22;-2198.145,138.4951;Inherit;False;21;warpBase;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;8;-2200,19.5;Inherit;False;3;3;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;29;-1913.314,475.9123;Inherit;False;2;2;0;FLOAT2;0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;18;-1869.077,20.68222;Inherit;False;2;2;0;FLOAT2;0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.FractNode;42;-1748.791,484.4885;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.FractNode;43;-1699.391,41.18841;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;7;-1525,-50.5;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;30;-1429.237,279.73;Inherit;False;Property;_Noise2GateWidth;Noise 2 Gate Width;11;0;Create;True;0;0;False;0;False;0.1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;31;-1405.237,206.7301;Inherit;False;Property;_Noise2Step;Noise 2 Step;10;0;Create;True;0;0;False;0;False;0.2;0.17;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;3;-1472,-260.5;Inherit;False;Property;_Noise1Step;Noise 1 Step;5;0;Create;True;0;0;False;0;False;0.2;0.32;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;32;-1569.237,404.73;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;4;-1487,-174.5;Inherit;False;Property;_Noise1GateWidth;Noise 1 Gate Width;6;0;Create;True;0;0;False;0;False;0.1;0.13;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;62;-1030.336,155.9032;Inherit;False;Property;_Noise1Scale;Noise 1 Inversion Scale;7;0;Create;False;0;0;False;0;False;1;100;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;37;-1247.237,406.73;Inherit;True;Property;_Noise2;Noise 2;8;0;Create;True;0;0;False;0;False;-1;3c5f973b007707340b4e0287f21d4469;51d0fd65fce003d408049ae3c28941b9;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;33;-1190.237,256.7301;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;5;-1171,-161.5;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;1;-1203,-48.5;Inherit;True;Property;_Noise1;Noise 1;3;0;Create;True;0;0;False;0;False;-1;3c5f973b007707340b4e0287f21d4469;51d0fd65fce003d408049ae3c28941b9;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;65;-1053.341,640.6572;Inherit;False;Property;_Noise2Scale;Noise 2 Inversion Scale;12;0;Create;False;0;0;False;0;False;1;4.29;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;61;-830.1561,575.9966;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;2;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;34;-841.2367,353.73;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;60;-802.5659,94.84607;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;2;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;2;-857.0184,-222.5005;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;35;-566.4498,443.5301;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;23;-535.5193,-43.1997;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;36;-340.5347,503.9444;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;24;-296.298,48.71439;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;38;-177.2922,278.2697;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;39;30.67054,209.5598;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;54;-24.20569,-143.0329;Inherit;False;Property;_Intensity;Intensity;1;0;Create;True;0;0;False;0;False;2;100;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;52;-75.24622,-57.7923;Inherit;False;Property;_Tint;Tint;0;0;Create;True;0;0;False;0;False;1,1,1,1;0,0.2745096,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;58;580.2476,-343.9582;Inherit;False;Property;_ParticleBlendDst;Destination Blend;19;1;[Enum];Create;False;0;1;UnityEngine.Rendering.BlendMode;True;0;False;10;10;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;53;282.7943,62.9671;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;57;574.2476,-416.9582;Inherit;False;Property;_ParticleBlendSrc;Source Blend;18;1;[Enum];Create;False;0;1;UnityEngine.Rendering.BlendMode;True;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;59;298.2439,232.0081;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;56;682.386,-179.7428;Inherit;False;Property;_CullMode;Cull Mode;17;1;[Enum];Create;True;0;1;UnityEngine.Rendering.CullMode;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;64;-3222.736,-41.39056;Inherit;False;Tangent;True;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ParallaxOffsetHlpNode;63;-3023.142,-94.1638;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0.02;False;2;FLOAT3;0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;592.1783,21.79755;Float;False;True;-1;2;ASEMaterialInspector;0;0;Unlit;Silent/Extra Magic;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;2;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;Transparent;;Transparent;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;1;1;True;57;10;True;58;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;2;-1;-1;-1;0;False;0;0;True;56;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;True;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;16;0;14;1
WireConnection;16;1;15;2
WireConnection;46;0;45;1
WireConnection;46;1;44;2
WireConnection;17;0;14;0
WireConnection;17;1;16;0
WireConnection;47;0;45;0
WireConnection;47;1;46;0
WireConnection;48;1;47;0
WireConnection;11;1;17;0
WireConnection;19;0;11;0
WireConnection;19;1;20;0
WireConnection;50;0;48;0
WireConnection;50;1;49;0
WireConnection;51;0;19;0
WireConnection;51;1;50;0
WireConnection;21;0;51;0
WireConnection;28;0;25;1
WireConnection;28;1;26;2
WireConnection;28;2;41;0
WireConnection;8;0;10;1
WireConnection;8;1;6;2
WireConnection;8;2;40;0
WireConnection;29;0;28;0
WireConnection;29;1;27;0
WireConnection;18;0;8;0
WireConnection;18;1;22;0
WireConnection;42;0;29;0
WireConnection;43;0;18;0
WireConnection;7;0;10;0
WireConnection;7;1;43;0
WireConnection;32;0;25;0
WireConnection;32;1;42;0
WireConnection;37;1;32;0
WireConnection;33;0;31;0
WireConnection;33;1;30;0
WireConnection;5;0;3;0
WireConnection;5;1;4;0
WireConnection;1;1;7;0
WireConnection;61;0;37;1
WireConnection;61;1;65;0
WireConnection;34;0;37;1
WireConnection;34;1;31;0
WireConnection;34;2;33;0
WireConnection;60;0;1;1
WireConnection;60;1;62;0
WireConnection;2;0;1;1
WireConnection;2;1;3;0
WireConnection;2;2;5;0
WireConnection;35;0;34;0
WireConnection;35;1;61;0
WireConnection;23;0;2;0
WireConnection;23;1;60;0
WireConnection;36;0;35;0
WireConnection;24;0;23;0
WireConnection;38;0;24;0
WireConnection;38;1;36;0
WireConnection;39;0;38;0
WireConnection;53;0;52;0
WireConnection;53;1;54;0
WireConnection;53;2;39;0
WireConnection;59;0;39;0
WireConnection;59;1;52;4
WireConnection;63;2;64;0
WireConnection;0;2;53;0
WireConnection;0;9;59;0
ASEEND*/
//CHKSM=58AE48E2C3DBAEE4F81175B6395D4F7A110F39B8